package com.ncb.customer.services.services;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

@Service
public class CustomerDetailsService {

	@Autowired
	DataSource dataSource;

	public List<?> getCustomerDetails(String customerNumber) {
		System.out.println("Request received for customer details with customer_id: " + customerNumber);
		System.out.println("Serving requests - version 2");
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("sp_getcustomerdetails").returningResultSet("results", new ColumnMapRowMapper());
		
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("cust_number", customerNumber);

		System.out.println("Returning the customer details with customer_id: " + customerNumber);
		return (List<?>) jdbcCall.execute(parameterSource).get("results");
	}

}
