package com.ncb.customer.services.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ncb.customer.services.services.CustomerDetailsService;

@RestController
@RequestMapping("/customers")
public class CustomerDetailsController {
	
	@Autowired
	CustomerDetailsService service;

	@GetMapping("/{customer_number}")
	public List<?> getCustomerDetails(@PathVariable("customer_number") String customerNumber) {

		return service.getCustomerDetails(customerNumber);
	}
}
